# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 11:59:24 2018

@author: enovi
"""

#Sentence similarity
#Return the number of matching sentences
#Enter number of words to match
from nltk import word_tokenize
from nltk.util import ngrams

def main():
    choice        = True
    article1_text = ''
    article2_text = ''
    c             = 'y'
    m_length      = 8
    
    print('Welcome to Sentence Similarity Analyzer!')
    while choice == True:
         c = input('Compare articles y/n? ').lower()
         if c == 'y':
             print('Enter the first message.')
             article1_text = get_text()
             print('Enter the second message.')
             article2_text = get_text()
             m_length = get_length()
             sentence_match(article1_text,article2_text,m_length)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def get_length():
    #Default is 8 words
    m_length = 8
    m_length = int(input('Enter the number of words to report a match: '))
    return m_length

def sentence_match(article1_text,article2_text,m_length):
    a1tokens    = word_tokenize(article1_text.lower())
    a2tokens    = word_tokenize(article2_text.lower())
    a1ngrams    = list(ngrams(a1tokens,m_length))
    a2ngrams    = list(ngrams(a2tokens,m_length))
    matches     = 0
    i           = 0
    j           = 0
    cyr_letters = ['а', 'с', 'е', 'о']
    for i in range(0,len(a1ngrams)):
        for j in range(0,len(a2ngrams)):
            if a1ngrams[i] == a2ngrams[j]:
                matches += 1
    print('The string length used for matching was {}.'.format(m_length))            
    print('The number of matches was {}.'.format(matches))            
    #detect cyrillic letters
    for word in article1_text:
        for letter in word:
            if letter in cyr_letters:
                print('Cyrillic letters detected in first text.')
    for word in article2_text:
        for letter in word:
            if letter in cyr_letters:
                print('Cyrillic letters detected in second text.')
                
main()